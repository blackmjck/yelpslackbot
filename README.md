# [Yelp Slackbot](https://bitbucket.org/blackmjck/yelpslackbot)
## The friendly restaurant finder

## Quick start

1. Clone repository to your local server
2. Create a new `tokens.json` file (or copy the `sample.tokens.json` file included in 
the repository) and populate this with your own API credentials.
3. Start the server by running `node server.js`
4. Register your server URL and running port with Slack with the settings described in `/slack_settings/SETTINGS.md`


## Features

* Uses the Slack custom command API to listen for requests using the `/hungryfor` slash command.
Queries the Yelp API for the top 20 nearby restaurants matching the search criteria and returns 
the data as Slack messages.


## Versioning
### v1.0 "Anchovy"
* Accepts command `/hungryfor foo` to show up to 20 relevant nearby restaurants matching "foo"
* Accepts command `/hungryfor foo near bar` to show up to 20 relevant nearby restaurants matching "foo" 
within the default radius of location "bar"
* Accepts distances in meters, kilometers, feet, yards, and miles.
* Accepts command `/hungryfor help` to show explanation of above

### v2.0 "Baked Potato"
* Caches channel location, radius, and sorting criteria in MongoDB indexed by channel ID/team ID for customization
* Adds additional accepted distance units.
* Accepts command `/hungryfor where` to show the currently set default location for the channel
* Accepts command `/hungryfor where foo` to set the location for the channel to "foo"
* Accepts command `/hungryfor distance` to show the currently set default search radius for the channel
* Accepts command `/hungryfor distance 20mi` to set the search radius for the channel to "20mi" (converts most 
length measurements, up to 40km maximum)
* Accepts command `/hungryfor sort` to show the currently set default sorting criteria for the channel
* Accepts command `/hungryfor sort (1|2|3|relevance|distance|rating)` to set the sorting criteria for the channel to 
one of the standard three options
* Accepts command `/hungryfor help (where|distance|sort)` to show usage information for the selected command


## License

The code is available under the [MIT license](LICENSE.txt).
