'use strict';

/*
 * Command endpoints:
 * - v1.0
 *   - /hungryfor ___ [near ___]
 *   - /hungryfor help
 * - v2.0
 *   - /hungryfor where [___]
 *   - /hungryfor distance [___]
 *   - /hungryfor sort [___]
 *   - /hungryfor help (where|distance|sort)
 */

var slack = require( './slack.js' ),
    bodyParser = require( 'body-parser' ),
    express = require( 'express' ),
    app = express(),
    port = process.env.PORT || 2222;

// middleware to populate the body within request objects
app.use( bodyParser.json() );
app.use( bodyParser.urlencoded( { extended: true } ) );

// handle Slack's certificate checking pings with a blank 200 response
app.get( '/', slack.handleAuthPing );

// handle POST requests to the default endpoint
app.post( '/', slack.handlePost );

// spillovers get a 404
app.use( slack.handle404 );

app.listen( port, function() {
    console.log( 'Slack <=> Yelp middleware server listening on port ' + port );
} );