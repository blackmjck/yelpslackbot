# Slack Custom Command Settings

Use the following responses to fill in the fields in your New Custom Command form to get integrated with Slack.


## "Integration Settings"

### Command: 
`/hungryfor`

### URL: 
`https://YOUR_SERVER_NAME/PATH_TO_ENDPOINT`

### Method: 
`POST`

### Token: 
(copy the provided value to "slack_token" in your `tokens.json` file)

### Customize Name: 
`HungryFor`

### Customize Icon: 
Use the `yelp-icon.png` provided in this folder

### Autocomplete Help Text: 
Make sure that `Show this command in the autocomplete list` is checked

### Autocomplete Help Text (Description): 
`Search for nearby restaurants by keyword`

### Autocomplete Help Text (Usage Hint): 
`[type of food you're hungry for]`

### Descriptive Label: 
`The friendly restaurant finder. Queries the Yelp API for the best nearby restaurants.`

