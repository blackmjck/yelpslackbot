'use strict';

var convert = require( 'convert-units' ),
    extend = require( 'util' )._extend,
    apiDefaults = {
        category_filter: 'restaurants,foodtrucks',
        limit: 10
    };

/**
 * Set the default API settings
 * TODO: for v2.0 start pulling these details from local MongoDB instance, indexed by 'client_id@team_id'
 */
process.env.yelp_radius = '15000';
process.env.yelp_sort = '1';
process.env.yelp_location = '5565 Centerview Dr, Raleigh, NC 27606';


/**
 * @name buildQuery
 * @param {String} terms  Comma-delineated string list of keywords to search
 * @param {String} [location]  Optional location string
 * @returns 
 */
function buildQuery( terms, location ) {
    return extend( apiDefaults, {
        term: terms,
        location: location || process.env.yelp_location,
        radius_filter: process.env.yelp_radius,
        sort: process.env.yelp_sort
    } )
}

/**
 * @name filterResults
 * @param {Array} arr  Array of businesses returned by the API call
 * @returns {Array}  Simplified array of business data, excluding closed locations
 */
function filterResults( arr ) {
    var results = [];

    if( arr.length ) {
        arr.forEach( function( r ) {
            if( !r.is_closed ) {
                results.push( {
                    name: r.name,
                    url: r.url,
                    description: r.snippet_text,
                    thumbnail: r.image_url,
                    rating: r.rating,
                    distance: convert( r.distance ).from( 'm' ).to( 'mi' ).toFixed( 3 ) + 'mi'
                } );
            }
        } );
    }

    return results;
}

module.exports = {
    buildQuery: buildQuery,
    filter: filterResults
};