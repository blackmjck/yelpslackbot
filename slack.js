/**
 * Slack request and response utilities
 */

'use strict';

var request = require( 'request' ),
    tokens = require( './tokens.json' ),
    yelp = require( 'node-yelp' ),
    yelpUtils = require( './yelp-utils.js' ),
    client = yelp.createClient( {
        oauth: {
            "consumer_key": tokens.yelp_key.toString(),
            "consumer_secret": tokens.yelp_secret.toString(),
            "token": tokens.yelp_token.toString(),
            "token_secret": tokens.yelp_token_secret.toString()
        },

        // Optional settings
        httpClient: {
            maxSockets: 25 // ~> Default is 10
        }
    } );

/**
 * @name authResponse
 * @param {*} req  Express-decorated http request object
 * @param {*} res  Express-decorated http response object
 * @param {Function} next
 * @description
 * If a Slack certificate check ping is detected, sends back an empty 200-OK response. Otherwise routes the request onward.
 */
function authResponse( req, res, next ) {
    // if this is a standard Slack ping-to-verify-certificate, give it the thumbs up
    if( req.body.ssl_check ) {
        res.status( 200 ).send( '' );
    } else {
        next();
    }
}

/**
 * @name handle404
 * @param {*} req  Express-decorated http request object
 * @param {*} res  Express-decorated http response object
 * @description
 * Sends back a 404-NOT FOUND response
 */
function handle404( req, res ) {
    //console.log( 'That\'s a 404.' );
    // anything that's gotten this far is looking for the wrong thing
    res.status( 404 ).send( 'Bad request' );
}

/**
 * @name handleHelpRequests
 * @param {String} command  The text of the command sent by the Slack user
 * @param {*} res  Express-decorated http response object
 * @description
 * Responds to help commands with usage details in an ephemeral Slack message
 */
function handleHelpRequests( command, res ) {
    // treat blank requests as help requests
    if( !command || command === 'help' || command === 'help near' ) {
        //console.log( 'Help me obi wan kenobi!' );
        return res.type( 'json' ).status( 200 ).send(
            slackResponse(
                'Use `/hungryfor keyword` to let our friendly Yelp gremlins find you nearby restaurants that match!',
                'You can also use `/hungryfor keyword near your_location` to search somewhere else.'
            ) );
    }
}

/**
 * @name handleInvalidToken
 * @param {Boolean} valid  Whether the request is valid
 * @param {*} res  Express-decorated http response object
 * @description
 * If token is invalid or missing, sends back a 401-UNAUTHORIZED response with an ephemeral Slack message
 */
function handleInvalidToken( valid, res ) {
    if( !valid ) {
        console.error( 'Token missing or invalid' );
        return res.type( 'json' ).status( 401 ).send( slackResponse( 'Sorry, that request was denied as unauthorized.' ) );
    }
}

/**
 * @name handleMissingFields
 * @param {JSON} body  Parsed body of the original http request
 * @param {*} res  Express-decorated http response object
 * @description
 * If team_id, channel_id, command, or response_url are missing, sends back a 400-BAD REQUEST response with an
 * ephemeral Slack message
 */
function handleMissingFields( body, res ) {
    if( !body.team_id || !body.channel_id || !body.command || !body.response_url ) {
        console.error( 'Missing fields in call.', Object.keys( body ) );
        return res.type( 'json' ).status( 400 ).send( slackResponse( 'API request was malformed!' ) );
    }
}

/**
 * @name handlePost
 * @param {*} req  Express-decorated http request object
 * @param {*} res  Express-decorated http response object
 * @description
 * Triages incoming request details and routes the request to the appropriate handler
 */
function handlePost( req, res ) {

    var body = req.body,
        token = body.token,
        isValid = token === tokens.slack_token.toString(),
        command = body.text.trim() || '';

    // 1. Handle token invalidity
    handleInvalidToken( isValid, res );

    // 2. Handle missing fields
    handleMissingFields( body, res );

    // 3. Handle 'help' requests
    handleHelpRequests( command, res );

    // 4. Handle standard search queries
    handleQueries( command, body.response_url );

    // only search queries get this far, which return delayed, so respond with a blank 200
    return res.type( 'json' ).status( 200 ).send( slackResponse( '', undefined, true ) );

}

/**
 * @name handleQueries
 * @param {String} command  The text of the command sent by the Slack user
 * @param {String} url  The Slack url to send responses to
 * @description
 * Queries the Yelp API for results and handles the return response to Slack
 */
function handleQueries( command, url ) {
    var query = command.split( ' near ' ),
        search = query[0].trim(),
        location = query[1] ? query[1].trim() : process.env.yelp_location;

    //console.info( 'Request received! Querying Yelp API for ' + search + ' near ' + location );

    // perform the search
    client.search(

        yelpUtils.buildQuery( search, location )

    ).then( function( data ) {

        var raw = data.businesses,
            results = yelpUtils.filter( raw );

        //console.info( 'Found ' + results.length + ' results.' );

        sendDetailedResponse( results, url )

    } ).catch( function( err ) {

        console.error( err );

        sendErrorResponse( err, url );

    } );
}

/**
 * @name sendDetailedResponse
 * @param {Array} results  Array of business results from the search query
 * @param {String} url  The Slack url to send responses to
 * @description
 * If results are found, wraps them up into a slack response with attachments and returns it in channel
 */
function sendDetailedResponse( results, url ) {

    // stick to a condensed version for now
    var response = wrapDetailedResponse( results, true );

    if( !results.length ) {
        return sendNoResults( url );
    }

    //console.info( 'Sending results to ' + url + ' =>', results );

    // send out actual results
    request( {
        method: 'POST',
        url: url,
        json: true,
        body: response
    }, function( err, response ) {
        if( err ) {
            console.error( err );
        } else {
            //console.info( 'Slack\'s return response:', response );
        }
    } );

}

/**
 * @name sendErrorResponse
 * @param {String} err  The text of the error returned by the API request
 * @param {String} url  The Slack url to send responses to
 * @description
 * Sends an ephemeral Slack response explaining the error
 */
function sendErrorResponse( err, url ) {
    // send back an error message
    request( {
        method: 'POST',
        url: url,
        json: true,
        body: slackWrapResponse( 'Sorry, there was error in the API request', err, false )
    }, function( err, response ) {
        if( err ) {
            console.error( err );
        } else {
            //console.info( 'Slack\'s return response:', response );
        }
    } );
}

/**
 * @name sendNoResults
 * @param {String} url  The Slack url to send responses to
 * @description
 * Sends an in-channel Slack response to indicate no results were found
 */
function sendNoResults( url ) {

    //console.info( 'Send no-results message to ' + url );

    request( {
        method: 'POST',
        url: url,
        json: true,
        body: slackWrapResponse( 'Sorry, there were no nearby results.', undefined, true )
    }, function( err, response ) {
        if( err ) {
            console.error( err );
        } else {
            //console.info( 'Slack\'s return response:', response );
        }
    } );
}

/**
 * @name slackResponse
 * @param {String} main  String to send as primary text
 * @param {*|String} [secondary]  String to send as attachment text
 * @param {Boolean} [channel=false]  Send message in channel? (default is false)
 * @returns {String} JSON-encoded response for POST back to slack
 */
function slackResponse( main, secondary, channel ) {
    return JSON.stringify( slackWrapResponse( main, secondary, channel ) );
}

/**
 * @name slackWrapResponse
 * @param {String} main  String to send as primary text
 * @param {*|String} secondary  String to send as attachment text
 * @param {Boolean} [channel=false]  Send message in channel? (default is false)
 * @returns {Object} Object in Slack reponse structured format
 */
function slackWrapResponse( main, secondary, channel ) {

    var resp = {
        "response_type": ( channel ? "in_channel" : "ephemeral" ),
        "text": main
    };

    if( secondary ) {
        resp.attachments = [
            {
                "text": secondary
            }
        ];
    }

    return resp;
}

/**
 * @name wrapDetailedResponse
 * @param {Array} results  Array of business information returned from the Yelp API
 * @param {Boolean} [condense=false]  Whether to use a condensed format rather than attachments for each business
 * @returns {Object} Response object ready for JSON stringifying for POST back to slack
 * @description
 * Builds a JSON response for Slack which lists the available responses and gives links
 */
function wrapDetailedResponse( results, condense ) {

    var response = {
        response_type: 'in_channel',
        text: 'Showing the top ' + results.length + ' results...',
        attachments: []
    };

    if( condense ) {

        var condensedList = '(in order of relevance)';

        results.forEach( function( biz ) {
            condensedList += [
                '\n<',
                biz.url,
                '|',
                biz.name,
                '>\n*Rating:* ',
                biz.rating,
                '\n*Distance:* ',
                biz.distance,
                '\n'
            ].join( '' );
        } );

        response.attachments.push( {
            text:      condensedList,
            mrkdwn_in: ['text']
        } );

    } else {

        results.forEach( function( biz ) {

            response.attachments.push( {
                color:      'good',
                fallback:   biz.name + ': ' + biz.url,
                title:      biz.name,
                title_link: biz.url,
                // adding description and thumbnail turned out to be way too busy onscreen
                //text: biz.description,
                //thumb_url: biz.thumbnail,
                fields:     [
                    {
                        title: 'Rating',
                        value: biz.rating,
                        short: true
                    },
                    {
                        title: 'Distance',
                        value: biz.distance,
                        short: true
                    }
                ]
            } );

        } );

    }

    return response;
}

module.exports = {
    handleAuthPing: authResponse,
    handle404: handle404,
    handlePost: handlePost
};
